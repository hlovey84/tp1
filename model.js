const {
  readFile,
  filterDataByTags,
  getRowById,
  saveSolution,
} = require("./helper");

const START = { id: 1, X: "2918", Y: "6528" };

const getDataFromFile = (pathFile) => {
  const data = readFile(pathFile);
  const dataArray = data.toString().replace("\r", "").split("\n");
  const demans = filterDataByTags(
    dataArray,
    "DEMANDAS",
    "FIN DEMANDAS",
    (value) => {
      return { id: value.split(" ")[0], deman: value.split(" ")[1] };
    }
  );
  const maxWeight = getRowById(dataArray, /CAPACIDAD/).replace("\r", "");
  const dimension = getRowById(dataArray, /DIMENSION/).replace("\r", "");
  const cordArray = filterDataByTags(
    dataArray,
    "NODE_COORD_SECTION",
    "EOF",
    (value) => {
      return {
        id: value.split(" ")[0],
        X: value.split(" ")[1],
        Y: value.split(" ")[2],
      };
    }
  );
  return { demans, maxWeight, dimension, cordArray };
};

const searchNextNode = (nodes, pathNotSolution) => {
  let minimalPath = { id: null, length: Number.MAX_VALUE };
  for (let i = 0; i < nodes.length; i++) {
    if (!pathNotSolution.includes(nodes[i].id)) {
      minimalPath =
        parseFloat(nodes[i].length) < parseFloat(minimalPath.length)
          ? nodes[i]
          : minimalPath;
    }
  }
  return minimalPath;
};

const getdemandByOfficeId = (demans, officeId) => {
  try {
    //console.log(officeId);
    return parseInt(demans.find((deman) => deman.id === officeId).deman);
  } catch (error) {
    console.log(error);
    throw new Error("Error");
    //return 1;
  }
};

const getMinimalPath = ({ src, path }) => {
  const { demans, maxWeight, dimension, cordArray } = getDataFromFile(path);
  let minimalPath = [];
  let weight = 0;
  let currentNode = src;
  let pathNotSolution = [];
  try {
    while (minimalPath.length < dimension) {
      const adjacentPermitted = cordArray.filter((val) => {
        const nodeId = val?.id;
        //const demon = getdemandByOfficeId(demans, nodeId);
        //let currentWeight = parseInt(weight) + parseInt(demon);
        return (
          //parseInt(currentWeight) <= parseInt(maxWeight) &&
          //parseInt(currentWeight) >= 0 &&
          !minimalPath.includes(val.id)
        );
      });
      console.log("TAMAÑO RUTA: ", minimalPath.length);
      const distancesFromCurrentOffice = adjacentPermitted.map((val) => {
        return {
          ...val,
          length: Math.sqrt(
            Math.pow(currentNode.X - val.X, 2) +
              Math.pow(currentNode.Y - val.Y, 2)
          ),
        };
      });
      if (distancesFromCurrentOffice.length == 0) {
        officeToExlude = minimalPath.pop();
        //weight -= parseInt(getdemandByOfficeId(demans, officeToExlude));
        pathNotSolution.push(officeToExlude);
      } else {
        const nextNode = searchNextNode(
          distancesFromCurrentOffice,
          pathNotSolution
        );
        minimalPath.push(nextNode.id);
        currentNode = nextNode;
        //weight += parseInt(getdemandByOfficeId(demans, nextNode.id));
        pathNotSolution = [];
        //console.log("Current Wigth:", weight);
      }
    }
    saveSolution(minimalPath);
  } catch (error) {
    console.log(error);
  }
};

getMinimalPath({ src: START, path: "problema_tres.txt" });
