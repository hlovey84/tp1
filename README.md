### Objetivo

Determinar el recorrido para visitar todas las sucursales, cumpliendo con la capacidad o carga minima y maxima del camion, con el fin de minimizar la distancia recorrida.

### Hipotesis

- Todas las sucursales estan conectadas entre si (grafo completo)
- La demanda de cada sucursal, asi como el peso o capacidad del camion, son enteros
- El recorrido se inicia desde una central, la cual no pertenece al archivo de problema
- 