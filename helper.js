const fs = require("fs");
const readFile = (path) => {
  try {
    const data = fs.readFileSync(path, "utf8");
    return data;
  } catch (error) {
    throw error;
  }
};

const filterDataByTags = (dataArray, initial_tag, last_tag, fnc) => {
  dataArray = dataArray.map((data) => {
    return data.replace("\r", "");
  });
  const initial_index = dataArray.indexOf(initial_tag);
  const last_index = dataArray.indexOf(last_tag);
  const filteredData = dataArray
    .filter((value, index) => {
      return index > initial_index && index < last_index;
    })
    .map((value) => {
      return fnc(value);
    });
  return filteredData;
};

const getRowById = (dataArray, regExp) => {
  const row = dataArray.find((value) => regExp.test(value)).split(" ")[1];
  return row;
};

const saveSolution = (solution) => {
  const file = fs.createWriteStream("solucion_tres.txt");
  solution.forEach((node) => {
    file.write(`${node} `);
  });
  file.end();
};

module.exports = { readFile, filterDataByTags, getRowById, saveSolution };
